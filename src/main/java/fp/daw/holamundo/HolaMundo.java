package fp.daw.holamundo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/hm")
public class HolaMundo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   public HolaMundo() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			PrintWriter out = new PrintWriter(response.getOutputStream());
			try {
				out.println("<!DOCTYPE html>");
				out.println("<html>");
				out.println("<head>");
				out.println("<meta charset=\"UTF-8\">");
				out.println("<title>Mi primera página web JEE</title>");
				out.println("</head>");
				out.println("<body>");
				out.println("	<h1>Hola Mundo desde un Servlet</h1>");
				out.println("</body>");
				out.println("</html>");
			} finally {
				out.close();
			}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
